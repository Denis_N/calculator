
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Denis Nikashin.
 * @version 1.0
 */
public class MainCalculator {

    public static void main(String[] args) throws IOException{

        String dividendString, dividerString;
        int dividend, divider;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please. Enter two numbers greater than zero");

        while (true) {
            dividendString = bufferedReader.readLine();
            dividend = Integer.parseInt(dividendString);

            if (dividend > 0.0) {
                break;
            } else {
                System.out.println("You enter the number: " + dividend);
                System.out.print("ATTENTION!!! The DIVIDEND can not be equal to or less than zero.");
                System.out.println("Try it again.");
            }
        }

        while (true) {
            dividerString = bufferedReader.readLine();
            divider = Integer.parseInt(dividerString);

            if (divider > 0.0) {
                break;
            } else {
                System.out.println("You enter the number: " + divider);
                System.out.print("ATTENTION!!! The DIVIDER can not be equal to or less than zero. ");
                System.out.println("Try it again.");
            }
        }

        while (dividend < divider) {
            System.out.printf("Please enter the dividend over the divisor!!!! \n Try again. \n");
            dividendString = bufferedReader.readLine();
            dividend = Integer.parseInt(dividendString);
            dividerString = bufferedReader.readLine();
            divider = Integer.parseInt(dividerString);
            if (dividend>divider)break;
        }
      devMethod(dividend, divider);
    }


    /**
    * The method executes line formation of the intermediate phase column subtraction
    * @param character contains a character string form.
    * @param counter receives the maximum shift of the border characters
    */
    private static void formatString (StringBuffer sb, char character, int counter) {
        for (int i = 0; i < counter; i++) sb.append(character);
    }

    /**
     * The method executes division of two positive numbers of type int
     */
     private static void devMethod(int dividend, int divider) {
        final int quotient = dividend / divider;
        final int modulo = dividend % divider;
        int multiplier = 1;
        int index;
        boolean isFirst = true;

        String dividendString = String.valueOf(dividend);
        String dividerString = String.valueOf(divider);
        final String quotientString = String.valueOf(quotient);
        final String moduloString= String.valueOf(modulo);
        StringBuffer printSB = new StringBuffer();

        final int dividendLength = dividendString.length();
        final int quotientLength = quotientString.length();

        System.out.printf(" %s | %s\n", dividendString, dividerString);

            for(index = 1; index < quotientLength; index++) {
                multiplier *= 10;
            }

            for(index = 1; index <= quotientLength; index++, isFirst = false, multiplier/=10 ){
                final int numberQuotient = Integer.parseInt(quotientString.substring(index -1, index));
                final int rightSpace = quotientLength - index;
                final int subtrahend = numberQuotient * divider;
                final int minuendLeftSpaces;
                final int subtrahendLeftSpace;
                final int limit;

                final String subtrahendString = String.valueOf(subtrahend);

                    if(numberQuotient != 0) {
                        String minuendString;

                        // the formation minuend line
                        if(isFirst){
                            minuendString = "";
                            minuendLeftSpaces = 0;
                        } else {
                            minuendString = String.valueOf(dividend);
                            minuendString = minuendString.substring(0, minuendString.length()-rightSpace);
                            minuendLeftSpaces = dividendLength - rightSpace - minuendString.length();
                            printSB.append(' ');
                            formatString(printSB, ' ', minuendLeftSpaces);
                            printSB.append(minuendString);
                            formatString(printSB, ' ', rightSpace);
                            printSB.append('\n');
                        }

                        // the formation subtracted line
                        subtrahendLeftSpace = dividendLength - rightSpace - subtrahendString.length();
                        formatString(printSB,' ', subtrahendLeftSpace);
                        printSB.append('-').append(subtrahendString);
                        formatString(printSB, ' ',rightSpace);

                        //the formation right separation line
                        if (isFirst){
                            printSB.append(" |-");
                            formatString(printSB, '-', quotientString.length());
                        }
                        printSB.append('\n');
                        printSB.append(' ');
                        formatString(printSB, ' ', minuendLeftSpaces);

                        //the formation of the resulting lines
                        if (isFirst){
                            limit = dividendLength - rightSpace;
                        }else{
                            limit = minuendString.length();
                        }
                        formatString(printSB,'-', limit);
                        formatString(printSB, ' ', rightSpace);

                        //the formation of the quotient placement
                        if (isFirst){
                            printSB.append(" | ").append(quotientString);
                        }
                        printSB.append('\n');
                    }
                dividend -= subtrahend * multiplier;

            }
         //the formation of the modulo placement
         printSB.append(' ');
        index = dividendLength - moduloString.length();
        formatString(printSB, ' ', index);
        printSB.append(moduloString);
        System.out.println(printSB.toString() + "\n");
        System.out.println("Your result: " + quotient + "; modulo: " + modulo);
    }
}
